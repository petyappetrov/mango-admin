import React from 'react'
import ReactDOM from 'react-dom'
import { createOvermind, IConfig } from 'overmind'
import { Provider } from 'overmind-react'
import { namespaced, merge } from 'overmind/config'
import { Routes } from './routes'
import './stylesheet.scss'

import { config as root } from './state-management'
import { config as auth } from './features/auth/state-management'
import { config as admins } from './features/admins/state-management'
import { config as users } from './features/users/state-management'
import { config as languages } from './features/languages/state-management'
import { config as words } from './features/words/state-management'

import { Notifications } from './ui/organisms/notifications'
import { useOvermind } from './hooks'

const features = namespaced({
  auth,
  admins,
  users,
  languages,
  words,
})

export const config = merge(root, features)

const overmind = createOvermind(config, {
  devtools: true,
  logProxies: true,
})

const App = () => {
  const { state, actions } = useOvermind()
  return (
    <>
      <Routes />
      <Notifications
        notifications={state.notifications}
        closeNotification={actions.closeNotification}
      />
    </>
  )
}

ReactDOM.render(
  <Provider value={overmind}>
    <App />
  </Provider>,
  document.getElementById('root'),
)

declare module 'overmind' {
  interface Config extends IConfig<typeof config> {}
}
