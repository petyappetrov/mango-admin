import axios, { AxiosResponse } from 'axios'
import { normalize, schema } from 'normalizr'
import { Word, WordFilters } from './words.state'
import { CreateWordValues } from '../ui/organisms/create-word-form'

interface NormalizedWords {
  result: string[]
  entities: {
    words: {
      [id: string]: Word
    }
  }
}

export const API = {
  async fetchWords(
    filters?: WordFilters,
  ): Promise<NormalizedWords & { count: number }> {
    const {
      data,
    }: AxiosResponse<{ items: Word[]; count: number }> = await axios.get(
      process.env.REACT_APP_API_ENDPOINT + '/words',
      {
        params: filters,
      },
    )
    const words = new schema.Entity('words', {}, { idAttribute: '_id' })

    const { result, entities }: NormalizedWords = normalize(data.items, [words])
    return { result, entities, count: data.count }
  },

  async fetchWord(word: string): Promise<Word> {
    const { data }: AxiosResponse<Word> = await axios.get(
      process.env.REACT_APP_API_ENDPOINT + `/words/${word}`,
    )
    return data
  },

  async removeWords(ids: string[]): Promise<string[]> {
    const { data }: AxiosResponse<Word[]> = await axios.delete(
      process.env.REACT_APP_API_ENDPOINT + '/words',
      {
        data: { ids },
      },
    )
    return data.map((word) => word._id)
  },

  async createWord(word: CreateWordValues) {
    const { data }: AxiosResponse<Word> = await axios.post(
      process.env.REACT_APP_API_ENDPOINT + '/words',
      word,
    )
    return data
  },

  async editWord({ _id, ...values }: Word) {
    const { data }: AxiosResponse<Word> = await axios.put(
      process.env.REACT_APP_API_ENDPOINT + `/words/${_id}`,
      values,
    )
    return data
  },
}
