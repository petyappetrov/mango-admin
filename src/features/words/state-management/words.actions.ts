import { AsyncAction, Action, pipe, mutate, filter, run } from 'overmind'
import qs from 'qs'
import { Word, WordFilters } from './words.state'
import { CreateWordValues } from '../ui/organisms/create-word-form'

export const fetchWords: AsyncAction<PageJS.Context> = pipe(
  filter(({ state }) => !state.words.isLoading),
  mutate(async ({ state, effects }) => {
    state.words.isLoading = true
    const { result, entities, count } = await effects.words.API.fetchWords(state.words.filters)
    state.words.allIds = result
    state.words.byId = entities.words
    state.words.count  = count
    state.words.isLoading = false
  }),
)

export const fetchWord: AsyncAction<string> = pipe(
  mutate(async ({ state, effects }, value: string) => {
    state.words.isLoading = true
    state.words.word = await effects.words.API.fetchWord(value)
    state.words.isLoading = false
  })
)

export const onSelect: Action<string[]> = pipe(
  mutate(({ state }, values) => {
    state.words.selected = values
  })
)

export const onRemoveSelectedWords: AsyncAction = pipe(
  mutate(async ({ state, effects }) => {
    const removedWordIds = await effects.words.API.removeWords(state.words.selected)
    state.words.allIds = state.words.allIds.filter((id) => !removedWordIds.includes(id))
    state.words.selected = []
  }),
  run(({ actions }) => {
    actions.showNotification({
      title: 'Успешно ✅',
      message: 'Выбранные слова были удалены',
    })
  }),
)

export const openEditWordForm: Action<string> = pipe(
  mutate(({ state }, id) => {
    state.words.editFormIsOpen = true
    state.words.editWordId = id
  })
)

export const closeEditWordForm: Action = pipe(
  mutate(({ state }) => {
    state.words.editFormIsOpen = false
    state.words.editWordId = undefined
  })
)

export const openCreateWordForm: Action = pipe(
  mutate(({ state }) => {
    state.words.createFormIsOpen = true
  })
)

export const closeCreateWordForm: Action = pipe(
  mutate(({ state }) => {
    state.words.createFormIsOpen = false
  })
)

export const openFiltersForm: Action = pipe(
  mutate(({ state }) => {
    state.words.filtersFormIsOpen = true
  })
)

export const closeFiltersForm: Action = pipe(
  mutate(({ state }) => {
    state.words.filtersFormIsOpen = false
  })
)

export const onCreateWord: Action<CreateWordValues> = pipe(
  mutate(async ({ state, effects }, values) => {
    state.words.isCreating = true
    const newWord = await effects.words.API.createWord(values)
    state.words.allIds = [
      ...state.words.allIds,
      newWord._id,
    ]
    state.words.byId = {
      ...state.words.byId,
      [newWord._id]: newWord,
    }
    state.words.isCreating = false
    state.words.createFormIsOpen = false
  }),
  run(({ actions }) => {
    actions.showNotification({
      title: 'Успешно ✅',
      message: 'Вы добавили новое слово',
    })
  }),
)

export const onEditWord: Action<Word> = pipe(
  mutate(async ({ state, effects }, word) => {
    state.words.isEditing = true
    const editedWord = await effects.words.API.editWord(word)
    state.words.byId[word._id] = editedWord
    state.words.isEditing = false
  }),
  run(({ actions }) => {
    actions.showNotification({
      title: 'Успешно ✅',
      message: 'Вы изменили слово',
    })
  }),
)

export const onFilterWords: Action<WordFilters> = pipe(
  mutate(({ state, effects }, values) => {
    state.words.filters = effects.utils.removeEmptyStrings<WordFilters>(values)
    effects.router.goTo(`/words?${qs.stringify(state.words.filters, { skipNulls: true })}`)
    state.words.filtersFormIsOpen = false
  })
)

export const onLoadMore: AsyncAction = pipe(
  mutate(async ({ state, effects }) => {
    state.words.isLoading = true
    const offset = state.words.allIds.length
    const limit = 30
    const { result, entities } = await effects.words.API.fetchWords({
      ...state.words.filters,
      offset,
      limit,
    })
    state.words.allIds = [...state.words.allIds, ...result]
    state.words.byId = {
      ...state.words.byId,
      ...entities.words
    }
    state.words.isLoading = false

  })
)
