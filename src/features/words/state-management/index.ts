import { state } from './words.state'
import * as actions from './words.actions'
import * as effects from './words.effects'

export const config = { state, actions, effects }
