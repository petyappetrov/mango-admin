import { Derive } from 'overmind'

export interface Word {
  readonly _id: string
  readonly createdAt: Date
  word: string
  translate: string
  examples: string[]
  language: string
  level: Level
}

export interface Level {
  readonly _id: string
  readonly name: string
  readonly code: string
  readonly createdAt: Date
}

export interface WordFilters {
  language?: string,
  search?: string
  level?: string
  limit?: number
  offset?: number
}

export type ById = {
  [id: string]: Word,
}

export type State = {
  byId: ById
  allIds: string[]
  count?: number
  filters?: WordFilters
  editWordId?: string,
  isLoading: boolean
  isRemoving: boolean
  items: Derive<State, Word[]>
  selected: string[]
  editFormIsOpen: boolean
  createFormIsOpen: boolean
  filtersFormIsOpen: boolean
  isCreating: boolean
  isEditing: boolean
  word?: Word
}

export const state: State = {
  byId: {},
  allIds: [],
  count: undefined,
  filters: undefined,
  editWordId: undefined,
  selected: [],
  items: (s) => s.allIds.map((id) => s.byId[id]),
  editFormIsOpen: false,
  createFormIsOpen: false,
  filtersFormIsOpen: false,
  isLoading: false,
  isRemoving: false,
  isCreating: false,
  isEditing: false,
  word: undefined,
}
