import React from 'react'
import { useOvermind } from 'hooks'
import { Loader, Text } from 'ui'

export const WordPage = () => {
  const { state } = useOvermind()

  if (state.words.isLoading) {
    return <Loader />
  }

  const word = state.words.word

  if (!word) {
    return null
  }

  return (
    <div>
      <Text>
        {word.word} - {word.translate}
      </Text>
      <div>
        {word.examples.map((example, i) => <Text key={i}>{example}</Text>)}
      </div>
    </div>
  )
}
