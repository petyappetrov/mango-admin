import React from 'react'
import { Row, Col } from 'react-flexbox-grid'
import { Title, Table, Button, Input } from 'ui'
import { useOvermind } from 'hooks'
import { Language } from '../../../languages/state-management/languages.state'
import { CreateWordForm } from '../organisms/create-word-form'
import { WordsFiltersForm } from '../organisms/words-filters-form'
import { FilteredItems } from '../molecules/filtered-items'
import { EditWordForm } from '../organisms/edit-word-form'
import { Level } from 'features/words/state-management/words.state'

export const WordsPage: React.FunctionComponent = () => {
  const { state, actions } = useOvermind()

  return (
    <>
      <Title>Слова</Title>
      <Row between='xs' style={{ marginBottom: 15 }}>
        <Col xs={8}>
          <Button
            style={{ marginRight: 15 }}
            onClick={actions.words.openCreateWordForm}
          >
            Добавить слово
          </Button>
          <Button variant='primary' onClick={actions.words.openFiltersForm}>
            Фильтры
            {state.words.filters && (
              <FilteredItems
                filters={state.words.filters}
                languages={state.languages.byId}
                levels={state.levelsOptions}
              />
            )}
          </Button>
        </Col>
        <Col xs={4}>
          <Row end='xs'>
            <Col xs={12}>
              <div>
                <Input
                  style={{ minWidth: 280, marginRight: 15 }}
                  placeholder='Поиск по слову'
                />
                <Button
                  variant='primary'
                  onClick={actions.words.openFiltersForm}
                >
                  Поиск
                </Button>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <Table
        isLoading={state.words.isLoading}
        data={state.words.items}
        onLoadMore={actions.words.onLoadMore}
        selected={state.words.selected}
        setSelected={actions.words.onSelect}
        count={state.words.count}
        onRemove={() => actions.words.onRemoveSelectedWords()}
        columns={[
          {
            header: 'Слово',
            accessor: 'word',
            transform: (col: React.ReactNode) => <a href={`/words/${col}`}>{col}</a>,
            width: 180,
          },
          {
            header: 'Перевод',
            accessor: 'translate',
            width: 260,
          },
          {
            header: 'Язык',
            accessor: 'language',
            transform: (language: Language) => {
              return language.code
            },
            width: 80,
          },
          {
            header: 'Уровень',
            accessor: 'level',
            width: 80,
            transform: (level: Level) => {
              return level.code
            }
          },
          {
            header: 'Edit',
            accessor: '_id',
            transform: (id) => {
              return <Button
                variant='primary'
                onClick={(event) => {
                  event.preventDefault()
                  event.stopPropagation()
                  actions.words.openEditWordForm(id)
                }}
              >
                Edit
              </Button>
            }
          }
        ]}
      />

      {state.words.createFormIsOpen && (
        <CreateWordForm
          closeCreateWordForm={actions.words.closeCreateWordForm}
          languagesOptions={state.languages.options}
          levelsOptions={state.levelsOptions}
          onCreateWord={actions.words.onCreateWord}
          filters={state.words.filters}
        />
      )}

      {state.words.filtersFormIsOpen && (
        <WordsFiltersForm
          closeFiltersForm={actions.words.closeFiltersForm}
          languagesOptions={state.languages.options}
          levelsOptions={state.levelsOptions}
          onFilterWords={actions.words.onFilterWords}
          filters={state.words.filters}
        />
      )}

      {state.words.editFormIsOpen && state.words.editWordId && (
        <EditWordForm
          word={state.words.byId[state.words.editWordId]}
          onEditWord={actions.words.onEditWord}
          closeEditWord={actions.words.closeEditWordForm}
          languagesOptions={state.languages.options}
          levelsOptions={state.levelsOptions}
        />
      )}
    </>
  )
}
