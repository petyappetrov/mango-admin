import React from 'react'
import { useForm } from 'react-hook-form'
import { Row, Col } from 'react-flexbox-grid'
import {
  Input,
  Select,
  InputGroup,
  Button,
  Text,
  Modal,
  ModalHeader,
  ModalContent,
  ModalFooter,
  Label,
} from '../../../../ui'
import { SelectOption } from '../../../../state-management/root.state'
import { WordFilters } from '../../state-management/words.state'

export const CreateWordForm: React.FunctionComponent<Props> = ({
  onCreateWord,
  closeCreateWordForm,
  languagesOptions,
  levelsOptions,
  filters = {},
}) => {
  const { register, handleSubmit, errors } = useForm<
    CreateWordValues
  >({
    defaultValues: {
      language: filters.language,
      level: filters.level,
    },
  })

  return (
    <Modal>
      <form onSubmit={handleSubmit(onCreateWord)}>
        <ModalHeader>Добавить слово</ModalHeader>
        <ModalContent>
          <Row>
            <Col xs={6}>
              <InputGroup>
                <Label>Слово</Label>
                <Input
                  fullWidth={true}
                  placeholder='Введите текст'
                  name='word'
                  ref={register({ required: 'Введите слово' })}
                />
                {errors.word && <Text>{errors.word.message}</Text>}
              </InputGroup>
            </Col>
            <Col xs={6}>
              <InputGroup>
                <Label>Перевод</Label>
                <Input
                  fullWidth={true}
                  placeholder='Введите текст'
                  name='translate'
                  ref={register({ required: 'Введите перевод слова' })}
                />
                {errors.translate && <Text>{errors.translate.message}</Text>}
              </InputGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <InputGroup>
                <Label>Пример</Label>
                <Input
                  fullWidth={true}
                  placeholder='Введите текст'
                  name='example'
                  ref={register}
                />
                {errors.example && <Text>{errors.example.message}</Text>}
              </InputGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={6}>
              <InputGroup>
                <Label>Язык</Label>
                <Select
                  name='language'
                  options={languagesOptions}
                  ref={register}
                />
              </InputGroup>
            </Col>
            <Col xs={6}>
              <InputGroup>
                <Label>Уровень</Label>
                <Select name='level' options={levelsOptions} ref={register} />
              </InputGroup>
            </Col>
          </Row>
        </ModalContent>
        <ModalFooter>
          <Button
            variant='primary'
            style={{ marginRight: 15 }}
            onClick={closeCreateWordForm}
          >
            Отменить
          </Button>
          <Button type='submit'>Добавить</Button>
        </ModalFooter>
      </form>
    </Modal>
  )
}

export interface CreateWordValues extends WordFilters {
  word: string
  translate: string
  example: string
}

interface Props {
  onCreateWord: (values: CreateWordValues) => void
  closeCreateWordForm: () => void
  languagesOptions: SelectOption[]
  levelsOptions: SelectOption[]
  filters?: WordFilters
}
