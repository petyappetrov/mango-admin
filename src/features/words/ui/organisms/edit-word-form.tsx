import React from 'react'
import { useForm } from 'react-hook-form'
import { Row, Col } from 'react-flexbox-grid'
import {
  Input,
  Select,
  InputGroup,
  Button,
  Text,
  Modal,
  ModalHeader,
  ModalContent,
  ModalFooter,
  Label,
} from '../../../../ui'
import { SelectOption } from '../../../../state-management/root.state'
import { Word } from '../../state-management/words.state'

export const EditWordForm: React.FunctionComponent<Props> = ({
  onEditWord,
  closeEditWord,
  languagesOptions,
  levelsOptions,
  word,
}) => {
  const { register, handleSubmit, errors } = useForm<
    Word
  >({
    defaultValues: {
      language: word.language,
      level: word.level,
      word: word.word,
      translate: word.translate,
      examples: word.examples,
    },
  })

  return (
    <Modal>
      <form onSubmit={handleSubmit((values) => onEditWord({ _id: word._id, ...values }))}>
        <ModalHeader>Редактировать слово</ModalHeader>
        <ModalContent>
          <Row>
            <Col xs={6}>
              <InputGroup>
                <Label>Слово</Label>
                <Input
                  fullWidth
                  placeholder='Введите текст'
                  name='word'
                  ref={register({ required: 'Введите слово' })}
                />
                {errors.word && <Text>{errors.word.message}</Text>}
              </InputGroup>
            </Col>
            <Col xs={6}>
              <InputGroup>
                <Label>Перевод</Label>
                <Input
                  fullWidth
                  placeholder='Введите текст'
                  name='translate'
                  ref={register({ required: 'Введите перевод слова' })}
                />
                {errors.translate && <Text>{errors.translate.message}</Text>}
              </InputGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <InputGroup>
                <Label>Пример</Label>
                <Input
                  fullWidth
                  placeholder='Введите текст'
                  name='example'
                  ref={register}
                />
                {errors.examples && <Text>{errors.examples.message}</Text>}
              </InputGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={6}>
              <InputGroup>
                <Label>Язык</Label>
                <Select
                  name='language'
                  options={languagesOptions}
                  ref={register}
                />
              </InputGroup>
            </Col>
            <Col xs={6}>
              <InputGroup>
                <Label>Уровень</Label>
                <Select name='level' options={levelsOptions} ref={register} />
              </InputGroup>
            </Col>
          </Row>
        </ModalContent>
        <ModalFooter>
          <Button
            variant='primary'
            style={{ marginRight: 15 }}
            onClick={closeEditWord}
          >
            Отменить
          </Button>
          <Button type='submit'>Добавить</Button>
        </ModalFooter>
      </form>
    </Modal>
  )
}

interface Props {
  onEditWord: (values: Word) => void
  closeEditWord: () => void
  languagesOptions: SelectOption[]
  levelsOptions: SelectOption[]
  word: Word
}
