import React from 'react'
import { useForm } from 'react-hook-form'
import { Row, Col } from 'react-flexbox-grid'
import {
  Select,
  InputGroup,
  Button,
  Modal,
  ModalHeader,
  ModalContent,
  ModalFooter,
  Label,
} from '../../../../ui'
import { SelectOption } from '../../../../state-management/root.state'
import { WordFilters } from '../../state-management/words.state'

export const WordsFiltersForm: React.FunctionComponent<Props> = ({
  closeFiltersForm,
  languagesOptions,
  levelsOptions,
  onFilterWords,
  filters = {},
}) => {
  const { register, handleSubmit } = useForm<WordFilters>({
    defaultValues: {
      language: filters.language,
      level: filters.level,
    }
  })

  return (
    <Modal>
      <form onSubmit={handleSubmit(onFilterWords)}>
        <ModalHeader>Изменить фильтры</ModalHeader>
        <ModalContent>
          <Row>
            <Col xs={6}>
              <InputGroup>
                <Label>Язык</Label>
                <Select
                  name='language'
                  options={languagesOptions}
                  ref={register}
                />
              </InputGroup>
            </Col>
            <Col xs={6}>
              <InputGroup>
                <Label>Уровень</Label>
                <Select
                  name='level'
                  options={levelsOptions}
                  ref={register}
                />
              </InputGroup>
            </Col>
          </Row>
        </ModalContent>
        <ModalFooter>
          <Button
            variant='primary'
            onClick={closeFiltersForm}
            style={{ marginRight: 15 }}
          >
            Отменить
          </Button>
          <Button type='submit'>Применить</Button>
        </ModalFooter>
      </form>
    </Modal>
  )
}

interface Props {
  closeFiltersForm: () => void
  onFilterWords: (values: WordFilters) => void
  languagesOptions: SelectOption[]
  levelsOptions: SelectOption[]
  filters?: WordFilters
}
