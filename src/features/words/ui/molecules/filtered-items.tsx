import React from 'react'
import { WordFilters } from '../../state-management/words.state'
import { Language } from '../../../languages/state-management/languages.state'
import { SelectOption } from '../../../../state-management/root.state'
import styles from './filtered-items.module.scss'

export const FilteredItems: React.FC<Props> = ({ filters, languages, levels }) => {
  const groupNames = {} as Groups

  if (filters.language) {
    groupNames.language = languages[filters.language].name.split(' ')[1]
  }

  if (filters.level) {
    const level = levels.find((l) => l.value === filters.level)
    groupNames.level = level && level.label.split(' ')[1]
  }

  if (!Object.keys(groupNames).length) {
    return null
  }
  return (
    <div className={styles.list}>
      {groupNames.language && (
        <span>{groupNames.language}</span>
      )}
      {groupNames.level && (
        <span>{groupNames.level}</span>
      )}
    </div>
  )
}

type Groups = {
  language?: string
  level?: string
}

interface Props {
  filters: WordFilters,
  languages: {
    [id: string]: Language
  }
  levels: SelectOption[]
}
