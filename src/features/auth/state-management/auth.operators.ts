import axios from 'axios'
import { catchError, Operator, mutate } from 'overmind'
// import { AxiosError } from 'axios'

export const checkErrors = (): Operator =>
  catchError(({ actions }) => {
    actions.showNotification({
      title: 'Серверная ошибка',
      message: 'Проверьте пожалуйста правильность заполнения полей',
    })
    // state.auth.errors = error.response.data
  })

export const saveTokenToStorage = (): Operator<string> =>
  mutate(({ effects, state }, token) => {
    axios.defaults.headers = {
      Authorization: `Bearer ${token}`,
    }
    effects.storage.set('token', token)
    state.auth.isAuthenticated = true
    state.auth.isLoading = false
    // state.auth.errors = null
    effects.router.goTo('/admins')
  })
