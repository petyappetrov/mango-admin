import axios, { AxiosResponse } from 'axios'
import { ILoginValues } from '../ui/organisms/login-form'
import { Profile } from './auth.state'

export const API = {
  async login(values: ILoginValues | void): Promise<string> {
    const { data }: AxiosResponse = await axios.post(
      process.env.REACT_APP_API_ENDPOINT + '/auth/login',
      values,
    )
    return data.token
  },

  async getProfile(): Promise<Profile> {
    const { data }: AxiosResponse = await axios.get(process.env.REACT_APP_API_ENDPOINT + '/auth/me')
    return data
  },
}
