import { state } from './auth.state'
import * as actions from './auth.actions'
import * as effects from './auth.effects'

export const config = { state, actions, effects }
