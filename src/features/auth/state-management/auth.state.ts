export interface Error {
  [field: string]: string
}

export interface Profile {
  email: string
  _id: string
}

export interface State {
  errors?: Error
  profile?: Profile
  isAuthenticated: boolean
  isLoading: boolean
}

export const state: State = {
  errors: undefined,
  isLoading: false,
  isAuthenticated: false,
  profile: undefined,
}
