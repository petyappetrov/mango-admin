import axios from 'axios'
import { Action, pipe, mutate, run, map, AsyncAction, catchError } from 'overmind'
import * as o from './auth.operators'
import { ILoginValues } from '../ui/organisms/login-form'
// import { Profile } from './auth.state'

export const login: Action<ILoginValues> = pipe(
  mutate(({ state }) => state.auth.isLoading = true),
  map(({ effects }, values) => effects.auth.API.login(values)),
  o.saveTokenToStorage(),
  run(({ actions }) => {
    actions.fetchInitialData()
    actions.showNotification({
      title: 'Добро пожаловать!',
      message: 'Вы успешно вошли в систему',
    })
  }),
  o.checkErrors(),
)

export const logout: Action = pipe(
  mutate(({ state }) => {
    state.auth.isAuthenticated = false
    axios.defaults.headers = {
      Authorization: null,
    }
  }),
  run(({ effects, actions }) => {
    effects.storage.remove('token')
    effects.router.goTo('/login')
    actions.showNotification({
      title: 'До скорой встречи!',
      message: 'Вы успешно вышли из системы',
    })
  }),
)

export const getProfile: AsyncAction = pipe(
  mutate(async ({ effects, state }) => {
    state.auth.profile = await effects.auth.API.getProfile()
  }),
  run(({ actions }) => {
    actions.showNotification({
      title: 'С возвращением!',
      message: 'Вы успешно прошли аутентификацию',
    })
  }),
  catchError(({ actions, effects }) => {
    effects.storage.remove('token')
    axios.defaults.headers = {
      Authorization: null,
    }
    actions.showNotification({
      title: 'Похоже у вас устарел токен!',
      message: 'Пожалуйста выполните вход повторно',
    })
  }),
)
