import React from 'react'
import { useForm } from 'react-hook-form'
import { Input, InputGroup, Button, Text, Link, Logo } from 'ui'
import { Error } from '../../state-management/auth.state'
import { HeaderForm } from '../molecules/header-form'
import { FooterForm } from '../molecules/footer-form'
import { Form } from '../molecules/form'

export interface ILoginValues {
  email: string
  password: string
}

interface Props {
  onLogin: (values: ILoginValues) => void
  errors?: Error
}

export const LoginForm: React.FunctionComponent<Props> = ({
  onLogin,
  errors: serverErrors,
}) => {
  const { register, handleSubmit, errors } = useForm<ILoginValues>()

  return (
    <Form onSubmit={handleSubmit(onLogin)}>
      <HeaderForm>
        <Logo />
        <Text>Выполните вход через свой аккаунт</Text>
      </HeaderForm>
      <InputGroup>
        <Input
          fullWidth={true}
          placeholder='Электронная почта'
          name='email'
          ref={register({
            required: 'Пожалуйста заполните электронную почту',
            pattern: {
              // eslint-disable-next-line
              value: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
              message: 'Неправильный формат email',
            },
          })}
        />
        {errors.email && <Text>{errors.email.message}</Text>}
      </InputGroup>
      <InputGroup>
        <Input
          fullWidth={true}
          placeholder='Пароль'
          name='password'
          type='password'
          ref={register({ required: 'Пожалуйста введите пароль' })}
        />
        {errors.password && <Text>{errors.password.message}</Text>}
      </InputGroup>
      <FooterForm>
        <Button
          variant='primary'
          type='submit'
          style={{ marginTop: 30, width: 220 }}
        >
          Войти
        </Button>
        <div>
          <Text style={{ marginTop: 30, fontSize: 14 }}>
            У тебя нет аккаунта? <br />
            Попроси инвайт у <Link href='/'>@petyappetrov</Link>
          </Text>
        </div>
      </FooterForm>
    </Form>
  )
}
