import React from 'react'
import styles from './auth-template.module.scss'

export const AuthTemplate: React.FunctionComponent = (props) => {
  return <div className={styles.root} {...props} />
}
