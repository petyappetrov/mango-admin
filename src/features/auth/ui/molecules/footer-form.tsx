import React from 'react'
import styles from './footer-form.module.scss'

export const FooterForm: React.FC = (props) => {
  return <div className={styles.footer} {...props} />
}
