import React from 'react'
import styles from './form.module.scss'

export const Form: React.FC<React.HTMLProps<HTMLFormElement>> = (props) => {
  return <form className={styles.form} {...props} />
}
