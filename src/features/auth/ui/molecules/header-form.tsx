import React from 'react'
import styles from './header-form.module.scss'

export const HeaderForm: React.FC = (props) => {
  return <div className={styles.header} {...props} />
}
