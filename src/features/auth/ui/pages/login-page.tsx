import React from 'react'
import { LoginForm } from '../organisms/login-form'
import { AuthTemplate } from '../templates/auth-template'
import { useOvermind } from '../../../../hooks'

export const LoginPage: React.FunctionComponent = () => {
  const { actions, state } = useOvermind()
  return (
    <AuthTemplate>
      <LoginForm onLogin={actions.auth.login} errors={state.auth.errors} />
    </AuthTemplate>
  )
}
