import { state } from './admins.state'
import * as actions from './admins.actions'
import * as effects from './admins.effects'

export const config = { state, actions, effects }
