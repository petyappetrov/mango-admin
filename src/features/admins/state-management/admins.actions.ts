import { AsyncAction } from 'overmind'

export const fetchAdmins: AsyncAction = async ({ state, effects }) => {
  state.admins.isLoading = true
  const { result, entities } = await effects.admins.API.fetchAdmins()
  state.admins.allIds = result
  state.admins.byId = entities.admins
  state.admins.isLoading = false
}
