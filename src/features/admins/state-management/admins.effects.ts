import axios, { AxiosResponse } from 'axios'
import { normalize, schema } from 'normalizr'
import { Admin } from './admins.state'

interface NormalizedAdmins {
  result: string[],
  entities: {
    admins: {
      [id: string]: Admin,
    },
  }
}

export const API = {
  async fetchAdmins(): Promise<NormalizedAdmins> {
    const { data }: AxiosResponse = await axios.get(process.env.REACT_APP_API_ENDPOINT + '/admins')
    const admins = new schema.Entity('admins', {}, { idAttribute: '_id' })

    const { result, entities }: NormalizedAdmins = normalize(data, [admins])
    return { result, entities }
  },
}
