import { Derive } from 'overmind'

export interface Admin {
  readonly _id: string
  readonly name: string
  readonly email: string
  readonly createdAt: string
}

export type State = {
  items: Derive<State, Admin[]>,
  byId: {
    [id: string]: Admin,
  },
  allIds: string[],
  isLoading: boolean,
}

export const state: State = {
  byId: {},
  allIds: [],
  items: (s) => s.allIds.map((id) => s.byId[id]),
  isLoading: false,
}
