import React from 'react'
import dayjs from 'dayjs'
import { Title, Table } from '../../../../ui'
import { useOvermind } from '../../../../hooks'

export const AdminsPage: React.FunctionComponent = () => {
  const { state } = useOvermind()
  const [selected, setSelected] = React.useState<string[]>([])

  return (
    <>
      <Title>Администраторы</Title>
      <Table
        isLoading={state.admins.isLoading}
        data={state.admins.items}
        selected={selected}
        setSelected={setSelected}
        columns={[
          {
            header: 'Имя',
            accessor: 'name',
            transform: (col: React.ReactNode) => <strong>{col}</strong>,
            width: 140,
          },
          {
            header: 'Email',
            accessor: 'email',
            width: 200,
          },
          {
            header: 'Дата регистрации',
            accessor: 'createdAt',
            width: 160,
            transform: (col: dayjs.ConfigType) =>
              dayjs(col).format('D MMMM, YYYY'),
          },
        ]}
      />
    </>
  )
}
