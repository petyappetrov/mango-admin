import React from 'react'
import dayjs from 'dayjs'
import { Title, Table } from '../../../../ui'
import { useOvermind } from '../../../../hooks'

export const LanguagesPage: React.FunctionComponent = () => {
  const { state, actions } = useOvermind()

  return (
    <>
      <Title>Языки</Title>
      <Table
        isLoading={state.languages.isLoading}
        data={state.languages.items}
        selected={state.languages.selected}
        setSelected={actions.languages.onSelect}
        columns={[
          {
            header: 'Имя',
            accessor: 'name',
            transform: (col: React.ReactNode) => <strong>{col}</strong>,
            width: 140,
          },
          {
            header: 'ISO Код',
            accessor: 'code',
            width: 200,
          },
          {
            header: 'Дата создания',
            accessor: 'createdAt',
            width: 160,
            transform: (col: dayjs.ConfigType) =>
              dayjs(col).format('D MMMM, YYYY'),
          },
        ]}
      />
    </>
  )
}
