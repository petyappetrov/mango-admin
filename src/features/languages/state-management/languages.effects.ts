import axios, { AxiosResponse } from 'axios'
import { normalize, schema } from 'normalizr'
import { Language } from './languages.state'

interface NormalizedLanguages {
  result: string[],
  entities: {
    languages: {
      [id: string]: Language,
    },
  }
}

export const API = {
  async fetchLanguages(): Promise<NormalizedLanguages> {
    const { data }: AxiosResponse = await axios.get(process.env.REACT_APP_API_ENDPOINT + '/languages')
    const languages = new schema.Entity('languages', {}, { idAttribute: '_id' })

    const { result, entities }: NormalizedLanguages = normalize(data, [languages])
    return { result, entities }
  },

  async removeLanguages(ids: string[]): Promise<Language[]> {
    const { data }: AxiosResponse = await axios.delete(process.env.REACT_APP_API_ENDPOINT + '/languages', {
      data: { ids },
    })
    return data
  },
}
