import { Derive } from 'overmind'
import { SelectOption } from '../../../state-management/root.state'

export interface Language {
  readonly _id: string
  readonly name: string
  readonly code: string
  readonly createdAt?: Date
}

export type State = {
  items: Derive<State, Language[]>
  options: Derive<State, SelectOption[]>
  byId: {
    [id: string]: Language
  }
  allIds: string[]
  selected: string[]
  isLoading: boolean
}

export const state: State = {
  byId: {},
  allIds: [],
  items: (s) => s.allIds.map((id) => s.byId[id]),
  options: (s) => [
    {
      label: 'Не выбран',
      value: '',
    },
    ...s.items.map((item) => ({ label: item.name, value: item._id })),
  ],
  selected: [],
  isLoading: false,
}
