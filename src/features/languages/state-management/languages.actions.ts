import { AsyncAction, Action, pipe, filter, mutate } from 'overmind'

export const fetchLanguages: AsyncAction<PageJS.Context | void> = pipe(
  filter(({ state }) => !state.languages.isLoading),
  mutate(async ({ state, effects }) => {
    state.languages.isLoading = true
    const { result, entities } = await effects.languages.API.fetchLanguages()
    state.languages.allIds = result
    state.languages.byId = entities.languages
    state.languages.isLoading = false
  }),
)

export const onRemoveLanguages: AsyncAction<string[]> = async ({ state, effects }, ids) => {
  state.languages.isLoading = true

  const removedlanguages = await effects.languages.API.removeLanguages(ids)
  state.languages.items = state.languages.items.filter((language) => removedlanguages.some((u) => u._id !== language._id))
  state.languages.selected = []
  state.languages.isLoading = false
}

export const onSelect: Action<string[]> = ({ state }, values) => {
  state.languages.selected = values
}
