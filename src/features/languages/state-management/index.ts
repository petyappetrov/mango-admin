import { state } from './languages.state'
import * as actions from './languages.actions'
import * as effects from './languages.effects'

export const config = { state, actions, effects }
