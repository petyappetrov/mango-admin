import axios, { AxiosResponse } from 'axios'
import { normalize, schema } from 'normalizr'
import { User } from './users.state'

interface NormalizedUsers {
  result: string[],
  entities: {
    users: {
      [id: string]: User,
    },
  }
}

export const API = {
  async fetchUsers(): Promise<NormalizedUsers> {
    const { data }: AxiosResponse = await axios.get(process.env.REACT_APP_API_ENDPOINT + '/users')
    const users = new schema.Entity('users', {}, { idAttribute: '_id' })

    const { result, entities }: NormalizedUsers = normalize(data.items, [users])
    return { result, entities }
  },

  async removeUsers(ids: string[]): Promise<User[]> {
    const { data }: AxiosResponse = await axios.delete(process.env.REACT_APP_API_ENDPOINT + '/users', {
      data: { ids },
    })
    return data
  },
}
