import { state } from './users.state'
import * as actions from './users.actions'
import * as effects from './users.effects'

export const config = { state, actions, effects }
