import { AsyncAction, Action } from 'overmind'

export const fetchUsers: AsyncAction = async ({ state, effects }) => {
  state.users.isLoading = true
  const { result, entities } = await effects.users.API.fetchUsers()
  state.users.allIds = result
  state.users.byId = entities.users
  state.users.isLoading = false
}

export const onRemoveUsers: AsyncAction<string[]> = async ({ state, effects }, ids) => {
  state.users.isLoading = true

  const removedUsers = await effects.users.API.removeUsers(ids)
  state.users.items = state.users.items.filter((user) => removedUsers.some((u) => u._id !== user._id))
  state.users.selected = []
  state.users.isLoading = false
}

export const onSelect: Action<string[]> = ({ state }, values) => {
  state.users.selected = values
}
