import { Derive } from 'overmind'
import { Language } from '../../languages/state-management/languages.state'
import { Level } from '../../words/state-management/words.state'

export interface User {
  readonly _id: string
  readonly username: string
  readonly firstName: string
  readonly langauge?: Language
  readonly level?: Level
  readonly notificationsStart?: string
  readonly notificationsEnd?: string
  readonly status: 'enabled' | 'disabled'
}

export type ById = {
  [id: string]: User,
}

export type State = {
  items: Derive<State, User[]>,
  byId: ById,
  allIds: string[],
  isLoading: boolean,
  selected: string[],
}

export const state: State = {
  byId: {},
  allIds: [],
  items: (s) => s.allIds.map((id) => s.byId[id]),
  selected: [],
  isLoading: false,
}
