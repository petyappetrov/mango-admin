import React from 'react'
import dayjs from 'dayjs'
import { Title, Table } from '../../../../ui'
import { useOvermind } from '../../../../hooks'
import { Language } from '../../../languages/state-management/languages.state'
import { Level } from 'features/words/state-management/words.state'

export const UsersPage: React.FunctionComponent = () => {
  const { state, actions } = useOvermind()

  return (
    <>
      <Title>Пользователи</Title>
      <Table
        isLoading={state.users.isLoading}
        data={state.users.items}
        selected={state.users.selected}
        setSelected={actions.users.onSelect}
        onRemove={() => actions.users.onRemoveUsers(state.users.selected)}
        columns={[
          {
            header: 'Telegram',
            accessor: 'username',
            transform: (col: React.ReactNode) => <strong>@{col}</strong>,
            width: 140,
          },
          {
            header: 'Язык',
            accessor: 'language',
            transform: (language?: Language) => {
              if (!language) {
                return 'Не выбран'
              }
              return language.name
            },
            width: 180,
          },
          {
            header: 'Уровень',
            accessor: 'level',
            width: 140,
            transform: (level?: Level) => {
              if (!level) {
                return 'Не выбран'
              }
              return level.code
            },
          },
          {
            header: 'Часовой пояс',
            accessor: 'timezone',
            width: 160,
          },
          {
            header: 'Дата регистрации',
            accessor: 'createdAt',
            width: 160,
            transform: (col: dayjs.ConfigType) =>
              dayjs(col).format('D MMMM, YYYY'),
          },
          {
            header: 'Статус',
            accessor: 'status',
            width: 160,
            transform: (status) => status === 'enabled' ? '👍' : '👎'
          },
        ]}
      />
    </>
  )
}
