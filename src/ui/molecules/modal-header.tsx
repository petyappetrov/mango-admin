import React from 'react'
import styles from './modal-header.module.scss'

export const ModalHeader: React.FC = (props) => {
  return (
    <div className={styles.header} {...props} />
  )
}
