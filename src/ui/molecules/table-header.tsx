import React from 'react'
import { IColumn } from '../organisms/table'
import { TableCol } from '../atoms/table-col'
import { TableSelect } from '../atoms/table-select'
import styles from './table-header.module.scss'

export const TableHeader = ({
  columns,
  selected,
  setSelected,
  data,
}: Props) => {
  const isSelectedAll: boolean = data.every((item: { _id: string }) => selected.includes(item._id))

  return (
    <div className={styles.tableHeader}>
      <TableCol width={30}>
        <TableSelect
          active={isSelectedAll}
          onClick={() =>
            setSelected(
              isSelectedAll
                ? []
                : data.map((item: { _id: string }) => item._id),
            )
          }
        />
      </TableCol>
      {columns.map((col, i) => (
        <TableCol key={i} width={col.width}>
          <strong>{col.header}</strong>
        </TableCol>
      ))}
    </div>
  )
}

interface Props {
  columns: IColumn[]
  selected: string[]
  setSelected: (values: string[]) => void
  data: any
}
