import React from 'react'
import { TableCol } from '../atoms/table-col'
import { TableRow } from '../atoms/table-row'
import { TableSelect } from '../atoms/table-select'
import { IColumn } from '../organisms/table'
import styles from './table-body.module.scss'

export const TableBody: React.FunctionComponent<Props> = ({
  columns,
  data,
  selected,
  setSelected,
}) => {
  return (
    <div className={styles.tableBody}>
      {data.map((row: { [id: string]: any }) => {
        const isSelected = selected.includes(row._id)
        return (
          <TableRow
            key={row._id}
            onClick={() =>
              setSelected(
                isSelected
                  ? selected.filter((id) => id !== row._id)
                  : [...selected, row._id],
              )
            }
          >
            <TableCol width={30}>
              <TableSelect active={isSelected} />
            </TableCol>
            {columns.map((col, i) => {
              return (
                <TableCol key={i} width={col.width}>
                  {typeof col.transform === 'function'
                    ? col.transform(row[col.accessor], row)
                    : row[col.accessor]}
                </TableCol>
              )
            })}
          </TableRow>
        )
      })}
    </div>
  )
}

interface Props {
  columns: IColumn[]
  selected: string[]
  setSelected: (values: string[]) => void
  data: any
}
