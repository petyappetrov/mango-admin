import * as React from 'react'
import { INotification } from '../../state-management/root.state'
import styles from './notification.module.scss'

export const Notification: React.FC<Props> = ({
  title,
  message,
  id,
  closeNotification,
}) => {
  return (
    <div
      className={styles.notification}
      onClick={() => id && closeNotification(id)}
    >
      {title && <div className={styles.title}>{title}</div>}
      {message}
    </div>
  )
}

interface Props extends INotification {
  closeNotification: (id: string) => void
}
