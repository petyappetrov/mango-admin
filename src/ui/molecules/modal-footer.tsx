import React from 'react'
import styles from './modal-footer.module.scss'

export const ModalFooter: React.FC = (props) => {
  return (
    <div className={styles.footer} {...props} />
  )
}
