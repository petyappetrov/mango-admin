import React from 'react'
import styles from './modal-content.module.scss'

export const ModalContent: React.FC = (props) => {
  return (
    <div className={styles.content} {...props} />
  )
}
