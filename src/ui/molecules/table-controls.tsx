import React from 'react'
import { Loader } from '../atoms/loader'
import { Button } from '../atoms/button'
import { Text } from '../atoms/text'
import styles from './table-controls.module.scss'

export const TableControls: React.FunctionComponent<Props> = ({
  selected,
  onRemove,
  onEdit,
  onLoadMore,
  count,
  data,
  isLoading,
}) => {
  if (isLoading) {
    return <Loader />
  }
  return (
    <div className={styles.controls}>
      {count && count > data.length && (
        <div>
          <Button onClick={onLoadMore} style={{ marginRight: 15 }}>Загрузить ещё</Button>
          {count && (
            <Text style={{ marginRight: 15 }}>Загружено {data.length} из {count}</Text>
          )}
        </div>
      )}
      <div>
        {onRemove && selected.length && (
          <Button
            variant='primary'
            onClick={onRemove}
            style={{ marginRight: 15 }}
          >
            Удалить
          </Button>
        )}
      </div>
      {selected.length && <Text>Выбрано: {selected.length}</Text>}
    </div>
  )
}

interface Props {
  selected: string[]
  data: any
  onRemove?: () => void
  onEdit?: () => void
  onLoadMore?: () => void
  count?: number
  isLoading?: boolean
}
