import React from 'react'
import { INotification } from '../../state-management/root.state'
import { Notification } from '../molecules/notification'
import styles from './notifications.module.scss'

export const Notifications: React.FunctionComponent<Props> = ({
  notifications,
  closeNotification,
}) => {
  if (!notifications) {
    return null
  }

  return (
    <div className={styles.notifications}>
      {notifications.map((notification) => (
        <Notification
          key={notification.id}
          closeNotification={closeNotification}
          {...notification}
        />
      ))}
    </div>
  )
}

interface Props {
  notifications?: INotification[],
  closeNotification: (id: string) => void
}
