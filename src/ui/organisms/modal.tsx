import React from 'react'
import { createPortal } from 'react-dom'
import styles from './modal.module.scss'

export const Modal: React.FC = ({ children }) => {
  const element = React.useMemo(() => document.createElement('div'), [])

  React.useEffect((): () => void => {
    document.body.appendChild(element)
    return () => document.body.removeChild(element)
  }, [element])

  return createPortal(
    <div className={styles.overlay}>
      <div className={styles.content}>{children}</div>
    </div>,
    element,
  )
}
