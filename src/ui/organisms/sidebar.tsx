import React from 'react'
import { SidebarLink } from '../atoms/sidebar-link'
import { Logo } from '../atoms/logo'
import styles from './sidebar.module.scss'

export const Sidebar: React.FunctionComponent<Props> = ({ page, logout }) => {
  return (
    <div className={styles.sidebar}>
      <Logo />
      <SidebarLink href='/admins' active={page === 'admins'}>
        Администраторы
      </SidebarLink>
      <SidebarLink href='/users' active={page === 'users'}>
        Пользователи
      </SidebarLink>
      <SidebarLink href='/words' active={page === 'words'}>
        Слова
      </SidebarLink>
      <SidebarLink href='/languages' active={page === 'languages'}>
        Языки
      </SidebarLink>
      <SidebarLink onClick={logout} style={{ marginTop: 'auto' }}>
        Выйти
      </SidebarLink>
    </div>
  )
}

interface Props {
  page: string
  logout: () => void
}
