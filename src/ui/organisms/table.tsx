import React from 'react'
import { TableHeader } from '../molecules/table-header'
import { TableBody } from '../molecules/table-body'
import { TableControls } from '../molecules/table-controls'
import styles from './table.module.scss'

export function Table({
  columns,
  data,
  isLoading,
  selected,
  setSelected,
  onLoadMore,
  onRemove,
  onEdit,
  count,
}: Props) {
  return (
    <div className={styles.table}>
      <TableHeader
        selected={selected}
        setSelected={setSelected}
        columns={columns}
        data={data}
      />
      <TableBody
        selected={selected}
        setSelected={setSelected}
        columns={columns}
        data={data}
      />
      <TableControls
        count={count}
        data={data}
        isLoading={isLoading}
        onLoadMore={onLoadMore}
        onRemove={onRemove}
        onEdit={onEdit}
        selected={selected}
      />
    </div>
  )
}

export interface IColumn {
  header: string | React.ReactNode
  accessor: string
  width?: number
  transform?: (col: any, row: any) => string | React.ReactNode
}

export interface Props {
  columns: IColumn[]
  data: any
  isLoading?: boolean
  selected: string[]
  setSelected: (values: string[]) => void
  onRemove?: () => void
  onLoadMore?: () => void
  onEdit?: () => void
  count?: number
}
