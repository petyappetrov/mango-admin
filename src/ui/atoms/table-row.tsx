import React from 'react'
import styles from './table-row.module.scss'

export const TableRow: React.FC<React.HTMLProps<HTMLDivElement>> = (props) => {
  return <div className={styles.tableRow} {...props} />
}

