import React from 'react'
import styles from './table-col.module.scss'

export const TableCol: React.FC<Props> = ({ width = 'auto', ...props }) => {
  return <div style={{ width }} className={styles.tableCol} {...props} />
}

interface Props {
  width?: number | 'auto'
}
