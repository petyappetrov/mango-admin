import React from 'react'
import clsx from 'clsx'
import styles from './button.module.scss'

export const Button: React.FC<Props> = ({
  variant = 'default',
  fullWidth,
  type = 'button',
  ...props
}) => {
  return (
    <button
      className={clsx(styles.button, styles[variant])}
      style={{ width: fullWidth ? '100%' : 'auto' }}
      type={type}
      {...props}
    />
  )
}

interface Props extends React.HTMLProps<HTMLButtonElement> {
  fullWidth?: boolean
  variant?: 'default' | 'primary' | 'error',
  type?: 'submit' | 'reset' | 'button'
}
