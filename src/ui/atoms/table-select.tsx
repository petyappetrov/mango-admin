import React from 'react'
import clsx from 'clsx'
import styles from './table-select.module.scss'

export const TableSelect: React.FC<Props> = ({ active, ...props }) => {
  return (
    <div
      className={clsx(styles.button, { [styles.active]: active })}
      {...props}
    />
  )
}

interface Props extends React.HTMLProps<HTMLDivElement> {
  active?: boolean
}
