import React from 'react'
import clsx from 'clsx'
import styles from './sidebar-link.module.scss'

export const SidebarLink: React.FC<Props> = ({ active, ...props }) => {
  return (
    // eslint-disable-next-line
    <a
      className={clsx(styles.link, { [styles.active]: active })}
      {...props}
    />
  )
}

interface Props extends React.HTMLProps<HTMLAnchorElement> {
  active?: boolean
}
