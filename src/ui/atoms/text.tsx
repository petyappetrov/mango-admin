import React from 'react'
import styles from './text.module.scss'

export const Text: React.FC<React.HTMLProps<HTMLSpanElement>> = (props) => {
  return <span className={styles.text} {...props} />
}
