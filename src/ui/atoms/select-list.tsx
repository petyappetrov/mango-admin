import React from 'react'
import { Tag } from './tag'
import styles from './select-list.module.scss'

export const SelectList: React.FC<Props> = ({
  onChange,
  options,
  selected = [],
  ...props
}) => {
  return (
    <div className={styles.list} {...props}>
      {options.map((option) => {
        const isSelected = selected.includes(option.value)
        const handleAdd = () => onChange([...selected, option.value])
        const handleRemove = () =>
          onChange(selected.filter((value) => value !== option.value))
        const onClick = () => (isSelected ? handleRemove() : handleAdd())
        return (
          <Tag
            style={{ marginRight: 5, marginBottom: 10 }}
            isDisabled={!isSelected}
            key={option.value}
            onClick={onClick}
          >
            {option.label}
          </Tag>
        )
      })}
    </div>
  )
}

interface Option {
  value: string | number
  label: string
}

interface Props {
  options: Option[]
  selected?: (string | number)[]
  onChange: (options: (string | number)[]) => void
  fullWidth?: boolean
}
