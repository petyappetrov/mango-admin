import React from 'react'
import styles from './title.module.scss'

export const Title: React.FC<{ children: string }> = (props) => {
  return <div className={styles.title} {...props} />
}
