import React from 'react'
import styles from './checkbox.module.scss'

export const Checkbox = React.forwardRef<HTMLInputElement, Props>(
  ({ fullWidth, name, label, ...props }, ref) => {
    return (
      <span>
        <input
          ref={ref}
          className={styles.input}
          hidden
          type='checkbox'
          name={name}
          id={name}
          {...props}
        />
        <label
          htmlFor={name}
          style={{ width: fullWidth ? '100%' : 'auto' }}
          className={styles.label}
        >
          {label}
        </label>
      </span>
    )
  },
)

interface Props extends React.HTMLProps<HTMLInputElement> {
  fullWidth?: boolean
}
