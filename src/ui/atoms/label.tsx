import React from 'react'
import styles from './label.module.scss'

export const Label: React.FC<React.HTMLProps<HTMLLabelElement>> = (props) => {
  return <label className={styles.label} {...props} />
}
