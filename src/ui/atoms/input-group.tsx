import React from 'react'
import styles from './input-group.module.scss'

export const InputGroup: React.FC = (props) => {
  return <div className={styles.inputGroup} {...props} />
}
