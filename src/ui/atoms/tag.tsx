import React from 'react'
import styles from './tag.module.scss'
import clsx from 'clsx'

export const Tag: React.FC<Props> = ({ isDisabled, ...props }) => {
  return (
    <span
      className={clsx(styles.tag, { [styles.disabled]: isDisabled })}
      {...props}
    />
  )
}

interface Props extends React.HTMLProps<HTMLSpanElement> {
  isDisabled?: boolean
}
