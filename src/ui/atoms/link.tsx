import React from 'react'
import styles from './link.module.scss'

export const Link: React.FC<React.HTMLProps<HTMLAnchorElement>> = (props) => {
  // eslint-disable-next-line
  return <a className={styles.link} {...props} />
}
