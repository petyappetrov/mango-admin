import React from 'react'
import styles from './select.module.scss'

export const Select = React.forwardRef<HTMLSelectElement, Props>(
  ({ options, fullWidth, ...props }, ref) => {
    return (
      <select
        ref={ref}
        className={styles.select}
        style={{
          width: fullWidth ? '100%' : 'auto',
        }}
        {...props}
      >
        {options.map((option) => {
          return (
            <option value={option.value} key={option.value}>
              {option.label}
            </option>
          )
        })}
      </select>
    )
  },
)

interface Option {
  value: string | number
  label: string
}

interface Props extends React.HTMLProps<HTMLSelectElement> {
  options: Option[]
  fullWidth?: boolean
}
