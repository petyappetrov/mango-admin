import React from 'react'
import styles from './logo.module.scss'

export const Logo: React.FC = (props) => {
  return (
    <div className={styles.logo} {...props}>
      Mango<span>Bot</span>
    </div>
  )
}
