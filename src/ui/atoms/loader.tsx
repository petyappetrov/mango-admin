import React from 'react'
import styles from './loader.module.scss'

export const Loader: React.FC = (props) => {
  return <div className={styles.loader} {...props} />
}
