import React from 'react'
import styles from './input.module.scss'

export const Input = React.forwardRef<HTMLInputElement, Props>(
  ({ fullWidth, ...props }, ref) => {
    return (
      <input
        className={styles.input}
        style={{ width: fullWidth ? '100%' : 'auto' }}
        ref={ref}
        {...props}
      />
    )
  },
)

interface Props extends React.HTMLProps<HTMLInputElement> {
  fullWidth?: boolean
}
