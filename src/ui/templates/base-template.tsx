import React from 'react'
import styles from './base-template.module.scss'

export const BaseTemplate: React.FunctionComponent<Props> = ({
  children,
  sidebar,
}) => {
  return (
    <>
      <aside className={styles.sidebar}>{sidebar}</aside>
      <main className={styles.content}>{children}</main>
    </>
  )
}

interface Props {
  sidebar: React.ReactNode
  children?: React.ReactNode
}
