import { createHook } from 'overmind-react'
import { config } from '../index'

export const useOvermind = createHook<typeof config>()
