import page from 'page'

export const router = {
  goTo: (url: string) => page.show(url),
}

export const storage = {
  set: (name: string, value: any): void => {
    try {
      localStorage.setItem(name, value)
    } catch (error) {
      throw Error(error)
    }
  },
  remove: (name: string): void => {
    try {
      localStorage.removeItem(name)
    } catch (error) {
      throw Error(error)
    }
  },
  get: (name: string): string | null => {
    try {
      const item = localStorage.getItem(name)
      return item
    } catch (error) {
      throw Error(error)
    }
  },
}

export const utils = {
  getRandomUUID: (): string => {
    return Math.random()
      .toString(36)
      .substr(2, 9)
  },

  removeEmptyStrings: <T>(obj: T): T => {
    if (obj && typeof obj === 'object') {
      const newObj = {} as T
      for (const key in obj) {
        const value = obj[key]

        if (
          (typeof value === 'string' && value !== '') ||
          (typeof value === 'object')
        ) {
          newObj[key] = value
        }

      }
      return newObj
    }

    return obj
  },

  onlyUnique: <T>(array: T[]): T[] => {
    return array.filter((value, index, a) => a.indexOf(value) === index)
  }
}
