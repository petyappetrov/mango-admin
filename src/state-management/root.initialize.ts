import axios from 'axios'
import { OnInitialize } from 'overmind'
import page from 'page'

export const onInitialize: OnInitialize = ({ actions, effects, state }, intance) => {
  const token: string | null = effects.storage.get('token')
  if (token) {
    state.auth.isAuthenticated = true
    axios.defaults.headers = {
      Authorization: `Bearer ${token}`,
    }
    actions.fetchInitialData()
  }

  page('/admins', actions.showAdminsPage)
  page('/users', actions.showUsersPage)
  page('/languages', actions.showLanguagesPage)
  page('/words', actions.showWordsPage)
  page('/words/:word', actions.showWordPage)
  page('/login', actions.showLoginPage)
  page('/register', actions.showRegisterPage)
  page.start()
}
