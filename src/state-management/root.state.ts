export interface SelectOption {
  label: string
  value: string
}

export interface INotification {
  id?: string
  title?: string
  message: string
  variant?: 'default' | 'warning' | 'success'
}

export interface State {
  page?: string
  notifications: INotification[]
  isLoading: boolean
  levelsOptions: SelectOption[]
}

export const state: State = {
  page: undefined,
  isLoading: false,
  notifications: [],
  levelsOptions: [
    {
      label: 'Не выбран',
      value: '',
    },
    {
      label: 'Начинающий 🙂',
      value: 'beginner',
    },
    {
      label: 'Продвинутый 😎',
      value: 'advanced',
    },
  ],
}
