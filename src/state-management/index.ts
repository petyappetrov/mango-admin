import { state } from './root.state'
import * as actions from './root.actions'
import * as effects from './root.effects'
import { onInitialize } from './root.initialize'

export const config = { state, actions, effects, onInitialize }
