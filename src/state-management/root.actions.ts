import { pipe, mutate, Action, run, wait, map, filter, parallel } from 'overmind'
import qs from 'qs'
import { INotification } from './root.state'
import * as o from './root.operators'

export const fetchInitialData: Action = pipe(
  mutate(({ state }) => state.isLoading = true),
  parallel(
    run(({ actions }) => actions.auth.getProfile()),
    run(({ actions }) => actions.languages.fetchLanguages()),
  ),
  mutate(({ state }) => state.isLoading = false),
)

export const showLoginPage: Action = pipe(
  o.checkAuthenticated({
    yes: run(({ effects }) => effects.router.goTo('/')),
    no: o.setPage('login'),
  }),
)

export const showRegisterPage: Action = pipe(
  o.checkAuthenticated({
    yes: run(({ effects }) => effects.router.goTo('/')),
    no: o.setPage('register'),
  }),
)

export const showAdminsPage: Action = pipe(
  o.checkAuthenticated({
    yes: pipe(
      o.setPage('admins'),
      run(({ actions }) => actions.admins.fetchAdmins()),
    ),
    no: run(({ effects }) => effects.router.goTo('/login')),
  }),
)

export const showUsersPage: Action = pipe(
  o.checkAuthenticated({
    yes: pipe(
      o.setPage('users'),
      run(({ actions }) => actions.users.fetchUsers()),
    ),
    no: run(({ effects }) => effects.router.goTo('/login')),
  }),
)

export const showLanguagesPage: Action<PageJS.Context> = pipe(
  filter(({ state }) => state.auth.isAuthenticated),
  o.setPage('languages'),
  run(({ actions }, context) => actions.languages.fetchLanguages(context))
)

export const showWordsPage: Action<PageJS.Context> = pipe(
  filter(({ state }) => state.auth.isAuthenticated),
  o.setPage('words'),
  mutate(({ state }, context) => {
    state.words.filters = qs.parse(context.querystring, { ignoreQueryPrefix: true })
  }),
  run(({ actions }, context) => actions.words.fetchWords(context))
)

export const showWordPage: Action<PageJS.Context> = pipe(
  filter(({ state }) => state.auth.isAuthenticated),
  o.setPage('word'),
  run(({ actions }, context) => actions.words.fetchWord(context.params.word))
)

export const showNotification: Action<INotification> = pipe(
  map(({ effects }, notification) => ({
    ...notification,
    id: effects.utils.getRandomUUID(),
  })),
  mutate(({ state }, notification) => {
    state.notifications = [...state.notifications, notification]
  }),
  wait(4000),
  filter(({ state }, notification) => state.notifications.some((n) => n.id === notification.id)),
  run(({ actions }, notification) => actions.closeNotification(notification.id)),
)

export const closeNotification: Action<string> = pipe(
  mutate(({ state }, id) => {
    state.notifications = state.notifications.filter((n) => n.id !== id)
  }),
)
