import { mutate, Operator, fork } from 'overmind'

export const setPage:<T> (page: string) => Operator<T> = (value) =>
  mutate(({ state }) => (state.page = value))

export const checkAuthenticated: (paths: { [prop: string]: Operator }) => Operator = (paths) =>
  fork(({ state }) => {
    if (state.auth.isAuthenticated) {
      return 'yes'
    }
    return 'no'
  }, paths)
