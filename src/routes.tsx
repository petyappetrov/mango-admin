import React from 'react'
import page from 'page'
import { BaseTemplate, Sidebar, Loader } from './ui'
import { useOvermind } from './hooks'
import { AdminsPage } from './features/admins/ui'
import { UsersPage } from './features/users/ui'
import { LanguagesPage } from './features/languages/ui'
import { WordsPage, WordPage } from './features/words/ui'
import { LoginPage } from './features/auth/ui'

const routes: { [key: string]: React.FunctionComponent } = {
  admins: AdminsPage,
  users: UsersPage,
  login: LoginPage,
  languages: LanguagesPage,
  words: WordsPage,
  word: WordPage,
}

export const Routes: React.FunctionComponent = () => {
  const { state, actions } = useOvermind()

  if (state.isLoading) {
    return <Loader />
  }

  if (!state.page) {
    page.redirect('/admins')
    return null
  }

  if (state.page === 'login') {
    return React.createElement(routes[state.page])
  }

  return (
    <BaseTemplate
      sidebar={<Sidebar page={state.page} logout={actions.auth.logout} />}
    >
      {React.createElement(routes[state.page])}
    </BaseTemplate>
  )
}
