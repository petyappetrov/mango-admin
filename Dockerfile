# Stage 1 - the build process
FROM node:latest as build-deps
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json", "./"]
RUN npm install
COPY . ./
ENV NODE_ENV=production REACT_APP_API_ENDPOINT=http://178.128.175.164:3001
RUN npm run build

# Stage 2 - the production environment
FROM nginx:1.17.5-alpine
COPY --from=build-deps /usr/src/app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
